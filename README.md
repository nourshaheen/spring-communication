# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* install maven, install mysql DB 
* run `mvn clean install` to build project 
* run employee APIs by `mvn spring-boot:run`
* run employee UI by `mvn spring-boot:run`

### Tools ###

* spring boot, spring web, spring data jpa
* lombok , mapstruct 
* swagger, themeleaf 

### learing ###
* [Full Tutorial] (https://www.youtube.com/playlist?list=PLMkr7X9JBPp4OlZCl0dzk8nXVb3Qygmeo)
* [mybatis course] (https://www.youtube.com/playlist?list=PLMkr7X9JBPp594l1cWrCuvVj-gwYtvOng)
* [mapstruct course] (https://www.youtube.com/playlist?list=PLMkr7X9JBPp4fc02yWxKsbYdLyBeiO4wt)
* [Designing Data Intensive Applications] (https://www.youtube.com/playlist?list=PLTRDUPO2OmIljJwE9XMYE_XEgEIWZDCuQ)
* [@Async annotation] (https://www.linkedin.com/pulse/asynchronous-calls-spring-boot-using-async-annotation-omar-ismail/?trackingId=SOITOnZTd7IKqgCTxFvsBg%3D%3D)
* [spring-boot-best-practices-developers] (https://www.linkedin.com/pulse/spring-boot-best-practices-developers-omar-ismail/?trackingId=gBihkoCDHk%2Bwft4G1GhI2Q%3D%3D)
* [Spring Boot Persistence Layer] (https://www.youtube.com/playlist?list=PLMkr7X9JBPp4n2DTF4U1sgMTrsCRQonyG)
* [Sonar] https://www.linkedin.com/pulse/sonarqube-jacoco-configuration-springboot-maven-santosh-kumar-kar/
* [Persistence Best Practices] (https://drive.google.com/file/d/1-hTxQ9uw4tyvslu1TMNxzphKiHPBI7WO/view?usp=sharing)
* [Books] (https://drive.google.com/drive/folders/1pWU0TjR-oisZKwVu17LJAwMLiKhVnqN2?usp=sharing)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

